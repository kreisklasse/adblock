# deny-allow-list for adguard Home


Presently i run some blocklists on an odroid N2p with an AdGuard AddOn on Home Assistant and on an odroid XU4 with AdGuard Home installed. 
* [Home-Assistant](https://www.home-assistant.io/hassio/installation/) with
* Home Assistant [AdGuard Home](https://github.com/hassio-addons/addon-adguard-home) AddOn
* [AdGuard Home](https://adguard.com/de/adguard-home/overview.html)


DNS-Server:
* [NextDNS.io](https://nextdns.io) (with some blocklists and logging active)
* https://doh.ffmuc.net/dns-query  - [Freifunk München](https://ffmuc.net/wiki/doku.php?id=knb:dohdot) - DoT & DoH DNS Server - (no blocklist, no logging, DoH)
* https://wikimedia-dns.org/dns-query  - [Wikimedia DNS](https://meta.wikimedia.org/wiki/Wikimedia_DNS) - DoH DNS Server - (no blocklist, no logging, DoH)
* quic://unfiltered.adguard-dns.com  - [AdGuard](https://adguard-dns.io/de/public-dns.html) - (no blocklist, no logging, DoQ)